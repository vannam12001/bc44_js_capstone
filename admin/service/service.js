const BASE_URL = "https://641465ce9172235b8693acbe.mockapi.io/adminProduct";
var idSelected = null;

var productArr = [];
// var dataJson = localStorage.getItem("LIST_OF_PRODUCT_LOCAL");
// if (dataJson != null) {
//   var dataArr = JSON.parse(dataJson);
//   for (var i = 0; i < dataArr.length; i++) {
//     var item = dataArr[i];
//     var sp = new Phone(
//       item.name,
//       item.price,
//       item.screen,
//       item.backCamera,
//       item.frontCamera,
//       item.img,
//       item.desc,
//       item.type
//     );
//     productArr.push(sp);
//     console.log("productArr", productArr);
//   }
// }
function fetchDSSP() {
  batLoading();
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      renderDSSP(res.data.reverse());
      // var dataJson = JSON.stringify(res.data);
      // localStorage.setItem("LIST_OF_PRODUCT_LOCAL", dataJson);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
// xoa sp
fetchDSSP();
function xoaSP(id) {
  batLoading();
  axios({
    url: `https://641465ce9172235b8693acbe.mockapi.io/adminProduct/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDSSP();
      console.log(res);
      Toastify({
        text: "Xóa Thành Công",
        className: "info",
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
      }).showToast();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
// them sp
document.getElementById("btnAddPhone").onclick = function addPhoneForm() {
  var sp = layThongTinTuFrom();
  console.log(sp);
  var isValid = kiemTraRong("tbname", sp.name);
  isValid = isValid & kiemTraRong("tbprice", sp.price); //&& checkPrice(sp.price);
  isValid = isValid & kiemTraRong("tbscreen", sp.screen);
  isValid = isValid & kiemTraRong("tbbackCam", sp.backCamera);
  isValid = isValid & kiemTraRong("tbfrontCam", sp.frontCamera);
  isValid = isValid & kiemTraRong("tbimg", sp.img);
  isValid = isValid & kiemTraRong("tbtype", sp.type);
  isValid = isValid & kiemTraRong("tbdesc", sp.desc);
  if (!isValid) {
    return;
  }

  axios({
    url: BASE_URL,
    method: "POST",
    data: sp,
  })
    .then(function (res) {
      //gọi lại api lấy danh sách mới nhất từ server sau khi xóa thành công
      fetchDSSP();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
};
//sua sp
function suaSP(id) {
  idSelected = id;
  batLoading();
  axios({
    url: `https://641465ce9172235b8693acbe.mockapi.io/adminProduct/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
      console.log(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
//update sp
function capNhatPhone(id) {
  var sp = layThongTinTuFrom();
  var isValid = kiemTraRong("tbname", sp.name);
  isValid = isValid & kiemTraRong("tbprice", sp.price); //&& checkPrice(sp.price);
  isValid = isValid & kiemTraRong("tbscreen", sp.screen);
  isValid = isValid & kiemTraRong("tbbackCam", sp.backCamera);
  isValid = isValid & kiemTraRong("tbfrontCam", sp.frontCamera);
  isValid = isValid & kiemTraRong("tbimg", sp.img);
  isValid = isValid & kiemTraRong("tbtype", sp.type); //&& checkType(sp.type);
  isValid = isValid & kiemTraRong("tbdesc", sp.desc);
  if (!isValid) {
    return;
  }
  axios({
    //url: BASE_URL + "/" + idSelected,
    url: `${BASE_URL}/${idSelected}`,
    method: "PUT",
    data: layThongTinTuFrom(),
  })
    .then(function (res) {
      fetchDSSP();
      Toastify({
        text: "Update Thành Công",
        className: "info",
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
      }).showToast();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}
