function renderDSSP(spArr) {
  productArr = spArr;
  var contentHTML = "";
  for (var i = 0; i < spArr.length; i++) {
    var sp = spArr[i];
    var contentTr = `<tr>
        <td>${sp.id}</td>
        <td>${sp.name}</td>
        <td>${sp.price}</td>
        <td style="text-align: center"><img src=${sp.img} width="150" height="150"></td>
        <td>${sp.desc}</td>
        <td>
        <button onclick="xoaSP(${sp.id})" class="btn btn-danger" >Delete</button>
        <button 
               onclick="suaSP(${sp.id})"
               data-bs-toggle="modal" data-bs-target="#exampleModal"
               class="btn btn-secondary" >Update
        </button>

        </td>
        </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tablePhone").innerHTML = contentHTML;
}
// reset form
document.querySelector("#addPhoneForm").addEventListener("click", function () {
  document.getElementById("name").value = "";
  document.getElementById("price").value = "";
  document.getElementById("screen").value = "";
  document.getElementById("backCam").value = "";
  document.getElementById("frontCam").value = "";
  document.getElementById("img").value = "";
  document.getElementById("desc").value = "";
  document.getElementById("type").value = "";

  document.getElementById("tbname").innerHTML = "";
  document.getElementById("tbprice").innerHTML = "";
  document.getElementById("tbscreen").innerHTML = "";
  document.getElementById("tbbackCam").innerHTML = "";
  document.getElementById("tbfrontCam").innerHTML = "";
  document.getElementById("tbimg").innerHTML = "";
  document.getElementById("tbdesc").innerHTML = "";
  document.getElementById("tbtype").innerHTML = "";
});

function searchProductAdmin() {
  batLoading();
  var request = document.getElementById("txtSearch").value.trim();
  console.log(request);
  axios({
    url: BASE_URL,
    method: "GET",
    params: {
      name: request,
    },
  })
    .then((res) => {
      tatLoading();
      console.log(res.data);
      var result = res.data;
      renderDSSP(result);
      Toastify({
        text: "Successfully searched products",
        offset: {
          x: 50,
          y: 10,
        },
      }).showToast();
      console.log(res);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}

function ascending() {
  var arrSort1 = productArr.sort((a, b) => a.price - b.price);
  console.log(arrSort1);
  renderDSSP(arrSort1);
  // productArr.sort(compareValues("price", "desc"));
}
function decrease() {
  var arrSort2 = productArr.sort((a, b) => b.price - a.price);
  console.log(arrSort2);
  renderDSSP(arrSort2);
}
