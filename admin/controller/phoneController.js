function layThongTinTuFrom() {
  var name = document.getElementById("name").value;
  var price = document.getElementById("price").value * 1;
  var screen = document.getElementById("screen").value;
  var backCamera = document.getElementById("backCam").value;
  var frontCamera = document.getElementById("frontCam").value;
  var img = document.getElementById("img").value;
  var desc = document.getElementById("desc").value;
  var type = document.getElementById("type").value;
  //   lưu lại
  var sp = new Phone(
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );

  return sp;
}

function showThongTinLenForm(sp) {
  document.getElementById("name").value = sp.name;
  document.getElementById("price").value = sp.price;
  document.getElementById("screen").value = sp.screen;
  document.getElementById("backCam").value = sp.backCamera;
  document.getElementById("frontCam").value = sp.frontCamera;
  document.getElementById("img").value = sp.img;
  document.getElementById("desc").value = sp.desc;
  document.getElementById("type").value = sp.type;
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
