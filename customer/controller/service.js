export class Service {
  getPhones = async () => {
    try {
      const res = await axios({
        url: "https://6443a3b2466f7c2b4b57544c.mockapi.io/product",
        method: "GET",
      });
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };
  getPhoneById = async (id) => {
    try {
      const res = await axios({
        url: `https://6443a3b2466f7c2b4b57544c.mockapi.io/product/${id}`,
        method: "GET",
      });

      return res.data;
    } catch (err) {
      console.log(err);
    }
  };
}
